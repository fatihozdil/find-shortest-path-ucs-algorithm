import csv
from queue import PriorityQueue


class CityNotFoundError(Exception):
    def __init__(self, city):
        print("%s does not exist" % city)


# Implement this function to read data into an appropriate data structure.
def build_graph(path):
    graph = {}
    with open(path) as f:
        reader = csv.reader(f)
        next(reader) # skip header
        for row in reader:
            city1, city2, distance = row # unpack row
            if city1 not in graph:
                graph[city1] = {}
            if city2 not in graph:
                graph[city2] = {}
            graph[city1][city2] = int(distance)
            graph[city2][city1] = int(distance)
    return graph

# Implement this function to perform uniform cost search on the graph.
def uniform_cost_search(graph, start, end):
    if start not in graph:
        raise CityNotFoundError(start)
    if end not in graph:
        raise CityNotFoundError(end)

    visited = set()
    queue = PriorityQueue()
    queue.put((0, start, [start]))

    while not queue.empty():
        
        # get the node with the lowest cost
        (cost, current_node, path) = queue.get()

        if current_node == end:
            return (cost, path)

        visited.add(current_node)

        for neighbor in graph[current_node]:
            if neighbor not in visited:
                new_cost = cost + graph[current_node][neighbor]
                new_path = path + [neighbor]
                queue.put((new_cost, neighbor, new_path))

    raise CityNotFoundError(end)

# Implement main to call functions with appropriate try-except blocks
if __name__ == "__main__":
    path = input("Enter the path : ")
    start = input("Enter the start : ")
    end = input("Enter the end : ")

    try:
        graph = build_graph(path) 
        # test_samples = [
        #     ('İstanbul', 'Kayseri'),
        #     ('Trabzon', 'İzmir'),
        #     ('Çanakkale', 'Konya'),
        #     ('Balıkesir', 'Adana'),
        #     ('İstanbul', 'Paris')
        # ]
        # for start, end in test_samples:
        #     cost, path = uniform_cost_search(graph, start, end)
        #     print("The shortest path is", path)
        #     print("The distance between", start, "and", end, "is", cost)
        
        cost, path = uniform_cost_search(graph, start, end)
        print("The shortest path is", path)
        print("The distance between", start, "and", end, "is", cost)
    except FileNotFoundError:
        print("File not found")
    except CityNotFoundError as e:
        e
   
